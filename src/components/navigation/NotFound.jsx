import React from 'react';

export default () => (
  <div>
    <h1>404</h1>
    <span>Not found :(</span>
  </div>
);