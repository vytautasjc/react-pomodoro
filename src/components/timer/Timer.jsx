import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { resetTimer, startTimer, stopSound, stopTimer } from './actions';

function padNumber(number) {
  return (`0${number}`).slice(-2);
}

function formatTime(durationSeconds) {
  const minutes = Math.floor(durationSeconds / 60);
  const seconds = durationSeconds - (minutes * 60);
  return `${padNumber(minutes)}:${padNumber(seconds)}`;
}

const Timer = (props) => {
  const { durationSeconds, currentSeconds, interval } = props;
  const isActive = !!interval;

  return (
    <div className="timer">
      <span className="time">{formatTime(durationSeconds - currentSeconds)}</span>
      <div className="controls">
        {
          !isActive &&
          <button onClick={() => props.startTimer(durationSeconds)}>Start</button>
        }
        {
          isActive &&
          <button onClick={props.stopTimer}>Stop</button>
        }
        <button className="reset" onClick={props.resetTimer}>Reset</button>
      </div>
    </div>
  );
};

Timer.propTypes = {
  interval: PropTypes.number,
  durationSeconds: PropTypes.number.isRequired,
  currentSeconds: PropTypes.number.isRequired,
  startTimer: PropTypes.func.isRequired,
  stopTimer: PropTypes.func.isRequired,
  resetTimer: PropTypes.func.isRequired
};

Timer.defaultProps = {
  interval: null
};

function mapStateToProps(state) {
  return { ...state.timer };
}

export default connect(mapStateToProps, {
  startTimer, stopTimer, resetTimer, stopSound
})(Timer);