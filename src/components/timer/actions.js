import * as ACTIONS from './types';

export const stopTimer = () => ({
  type: ACTIONS.STOP_TIMER
});

export const resetTimer = () => ({
  type: ACTIONS.RESET_TIMER
});

export const stopSound = () => ({
  type: ACTIONS.STOP_SOUND
});

export const startTimer = durationSeconds => (dispatch, getState) => {
  const state = getState();
  if (state.interval) {
    return;
  }
  const currentSeconds = state.timer.currentSeconds || 0;
  const interval = setInterval(() => {
    const updatedState = getState();
    if (updatedState.timer.currentSeconds === durationSeconds) {
      dispatch({ type: ACTIONS.FINISH_TIMER });
    } else {
      dispatch({ type: ACTIONS.TICK_TIMER });
    }
  }, 1000);

  dispatch({
    type: ACTIONS.START_TIMER,
    durationSeconds,
    currentSeconds,
    interval
  });
};