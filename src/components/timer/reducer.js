import * as ACTIONS from './types';

const initialState = {
  currentSeconds: 0,
  interval: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.START_TIMER:
      return {
        ...state,
        currentSeconds: action.currentSeconds,
        interval: action.interval
      };

    case ACTIONS.STOP_TIMER:
      if (state.interval) {
        clearInterval(state.interval);
      }

      return {
        ...state,
        interval: null
      };

    case ACTIONS.TICK_TIMER:
      return {
        ...state,
        currentSeconds: state.currentSeconds + 1
      };

    case ACTIONS.RESET_TIMER:
      if (state.interval) {
        clearInterval(state.interval);
      }

      return {
        ...state,
        currentSeconds: 0,
        interval: null
      };

    case ACTIONS.FINISH_TIMER:
      if (state.interval) {
        clearInterval(state.interval);
      }

      return {
        ...state,
        ...initialState
      };

    default:
      return state;
  }
};