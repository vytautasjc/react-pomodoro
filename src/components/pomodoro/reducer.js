import * as TIMER_ACTIONS from '../timer/types';

const POMODORO_COUNT = 4;

function minutesToSeconds(mins) {
  return mins * 60;
}

const TIMER_CONFIG = {
  POMODORO: {
    name: 'Pomodoro',
    durationSeconds: minutesToSeconds(25)
  },
  SHORT_BREAK: {
    name: 'Short Break',
    durationSeconds: minutesToSeconds(5)
  },
  LONG_BREAL: {
    name: 'Long Break',
    durationSeconds: minutesToSeconds(15)
  }
};


function resolveTimerConfig(state, pomodoroCount) {
  if (pomodoroCount === POMODORO_COUNT) {
    return TIMER_CONFIG.LONG_BREAL;
  }

  const timerConfig = state.timerConfig;
  return timerConfig === TIMER_CONFIG.POMODORO ? TIMER_CONFIG.SHORT_BREAK : TIMER_CONFIG.POMODORO;
}

function handleFinishedTimer(state) {
  const history = state.history || [];

  history.push(state.timerConfig.name);

  let pomodoroCount = state.pomodoroCount || 0;
  if (state.timerConfig.name === TIMER_CONFIG.POMODORO.name) {
    pomodoroCount += 1;
  }

  return {
    ...state,
    history,
    timerConfig: resolveTimerConfig(state, pomodoroCount),
    pomodoroCount
  };
}

const initialState = {
  history: null,
  pomodoroCount: 0,
  timerConfig: TIMER_CONFIG.POMODORO
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TIMER_ACTIONS.FINISH_TIMER:
      return handleFinishedTimer(state);

    default:
      return state;
  }
};