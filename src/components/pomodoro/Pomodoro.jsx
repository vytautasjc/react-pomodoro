import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Timer from '../timer/Timer';
import History from './History';

const Pomodoro = (props) => {
  const { timerConfig, history } = props;
  return (
    <div>
      <h1>{timerConfig.name}</h1>
      <Timer durationSeconds={timerConfig.durationSeconds}/>
      <History history={history}/>
    </div>
  );
};

Pomodoro.propTypes = {
  timerConfig: PropTypes.shape({
    name: PropTypes.string.isRequired,
    durationSeconds: PropTypes.number.isRequired
  }).isRequired,
  history: PropTypes.arrayOf(PropTypes.string)
};

Pomodoro.defaultProps = {
  history: null
};

const mapStateToProps = state => ({ ...state.pomodoro });

export default connect(mapStateToProps)(Pomodoro);