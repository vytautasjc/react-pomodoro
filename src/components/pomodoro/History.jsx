import React from 'react';
import PropTypes from 'prop-types';

const History = props => (
  props.history &&
  <div className="history">
    <h1>History</h1>
    <div className="history-list">
      <ul>
        {props.history.map((item, index) => <li key={index}>{item}</li>)}
      </ul>
    </div>
  </div>
);

History.propTypes = {
  history: PropTypes.arrayOf(PropTypes.string)
};

History.defaultProps = {
  history: null
};

export default History;