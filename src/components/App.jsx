import React from 'react';
import {BrowserRouter as Router, Link, Redirect, Route, Switch} from 'react-router-dom';
import Pomodoro from './pomodoro/Pomodoro';
import Home from './home/Home';
import NotFound from './navigation/NotFound';

const App = () => (
  <Router>
    <div>
      <div className="nav-bar">
        <ul>
          <li>
            <Link to="/pomodoro/home">Home</Link>
          </li>
          <li>
            <Link to="/pomodoro">Pomodoro</Link>
          </li>
          <li>
            <Link to="/pomodoro2">Some not found link</Link>
          </li>
        </ul>
        <hr/>
      </div>

      <div className="content">
        <Switch>
          <Route exact path="/pomodoro" component={Pomodoro}/>
          <Route path="/pomodoro/home" component={Home}/>
          <Redirect exact from="/" to="/pomodoro/home"/>
          <Route component={NotFound}/>
        </Switch>
      </div>
    </div>
  </Router>
);

export default App;