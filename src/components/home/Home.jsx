import React from 'react';

const Home = () => (
  <div>
    <h1>Home</h1>
    <span>This is a simple pomodoro timer app.</span>
  </div>
);

export default Home;