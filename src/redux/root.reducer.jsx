import { combineReducers } from 'redux';

import pomodoroReducer from '../components/pomodoro/reducer';
import timerReducer from '../components/timer/reducer';

export default combineReducers({ pomodoro: pomodoroReducer, timer: timerReducer });