# React Pomodoro
### Installing
```
yarn install
```
### Building
This defaults to development config. Add new configs and npm scripts in package.json for production as needed.
```
yarn build
```
### Running
This defaults to development config. Add new configs and npm scripts in package.json for production as needed.
```
yarn start
```

### Sounds
This project uses these sounds from freesound:  
_Bell, Candle Damper, A (H1).wav_ by InspectorJ ( https://freesound.org/people/InspectorJ/ )