const makeWebpackConfig = require('./make.webpack.config');

module.exports = makeWebpackConfig({
  mode: 'development',
  devtool: false,
  devServer: {
    contentBase: './build',
    historyApiFallback: true
  }
});
