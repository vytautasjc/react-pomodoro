const HtmlPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');

const BUILD_DIR_NAME = 'build';

const config = {
  entry: [
    './src/index.jsx'
  ],
  output: {
    path: path.resolve(__dirname, BUILD_DIR_NAME),
    publicPath:
      '/',
    filename:
      'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.jsx$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.wav$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  plugins: [
    new CleanWebpackPlugin([BUILD_DIR_NAME]),
    new HtmlPlugin({
      template: './resources/index.html'
    })
  ]
};

module.exports = function (overrides) {
  return Object.assign(config, overrides);
};